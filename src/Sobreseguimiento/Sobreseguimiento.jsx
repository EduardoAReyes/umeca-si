import React from "react";
import Header from "../Header";
import SobreseguimientoMenu from "./SobreseguimientoMenu";
function Sobreseguimiento() {
  return (
    <>
      <Header encabezado="SOBRESEGUIMIENTO" />
      <SobreseguimientoMenu />
    </>
  );
}

export default Sobreseguimiento;
