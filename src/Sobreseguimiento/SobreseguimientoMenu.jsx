import React, { useState } from "react";
import ModalAsistencia from "./ModalAsistencia";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import ModalLista from "./ModalLista";

function SobreseguimientoMenu() {
  const navegar = useNavigate();
  const [iniciarVideo, setIniciarVideo] = useState(false);
  const [modalAsistenciaShow, setModalAsistenciaShow] = useState(false);
  const [modalListaShow, setModalListaShow] = useState(false);
  return (
    <>
      <style>{`
                      .container{
                        max-width: 1200px;
                        margin: 20px auto;
                      }
                      a{
                        cursor:pointer;
                      }
      
                      .links {
                          text-align: center;
                          margin-bottom: 20px;
                          
                      }
      
                      .links a {
                          display: block;
                          margin-bottom: 10px;
                          font-weight: bold;
                          color: #007bff;
                          text-decoration: none;
                          transition: color 0.3s ease;
                      }
      
                      .links a i {
                          margin-right: 5px;
                      }
      
                      .links a:hover {
                          color: #0056b3;
                          text-decoration: none;
                          transform: scale(1.1);
                          transition: transform 0.3s ease;
                      }

        .flecha-a-menu{
          position:fixed;
          width:50px;
        }
    `}</style>
      <Link to="/Menu">
        <img src="arrow.svg" className="flecha-a-menu" />
      </Link>
      <ModalAsistencia
        show={modalAsistenciaShow}
        onHide={() => {
          setModalAsistenciaShow(false);
        }}
        iniciarVideo={iniciarVideo}
        setIniciarVideo={setIniciarVideo}
      />

      <ModalLista
        show={modalListaShow}
        onHide={() => {
          setModalListaShow(false);
        }}
      />
      {/* <button
        onClick={() => {
          setModalAsistenciaShow(true);
          setIniciarVideo(true);
        }}
      >
        Asistencia
      </button>
      <button>Lista de asistencia</button> */}
      <div className="container">
        <div className="row">
          <div className="col-3 col-left"></div>
          <div className="col-6">
            <div className="links">
              <a
                onClick={() => {
                  setModalAsistenciaShow(true);
                  setIniciarVideo(true);
                }}
              >
                <i className="fas fa-users fa-lg">
                  <h1>Asistencia</h1>
                </i>
              </a>
              <a onClick={() => {
                setModalListaShow(true)
              }}>
                <i className="fas fa-users fa-lg">
                  <h1>Lista</h1>
                </i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SobreseguimientoMenu;
