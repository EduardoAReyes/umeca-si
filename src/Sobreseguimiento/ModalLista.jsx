import React from "react";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/table";
import Axios from "axios";
import { useEffect, useState } from "react";
function ModalLista({ show, onHide }) {
  const [ultimoCampo, setUltimoCampo] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarAsistenciaImputados")
      .then((res) => {
        setUltimoCampo(res.data);
      })
      .catch((err) => {
        console.log("Hubo un error", err);
      });
  }, []);

  return (
    <>
      <style>
        {`
      .tablaImp{
        max-height:300px;
        overflow-y:auto;
      }
      `}
      </style>
      <Modal
        show={show}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            LISTA DE ASISTENCIA
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="tablaImp">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Fecha</th>
                  <th>Hora</th>
                </tr>
              </thead>
              <tbody>
                {ultimoCampo.map((val) => (
                  <tr key={val.id_asistencia}>
                    <td>{val.nombre}</td>
                    <td>{val.fecha}</td>
                    <td>{val.hora}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => {
              onHide();
            }}
          >
            Cerrar
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalLista;
