import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Webcam from "react-webcam";
import { useRef, useState, useEffect } from "react";
import * as faceapi from "@vladmandic/face-api";
import Axios from "axios";

const videoConstraints = {
  width: 620,
  height: 460,
  facingMode: "user",
};

function ModalAsistencia({ show, onHide, iniciarVideo, setIniciarVideo }) {
  const webcamRef = useRef();
  // const videoRef = useRef();

  const [cargando, setcargando] = useState(false);

  const modelPath = "/assets/lib/face-api/models";
  const minScore = 0.5;
  const maxResults = 100;
  let optionsSSDMobileNet;

  const [stream, setStream] = useState(null);
  const videoRef = useRef(null);
  const canvasRef = useRef(null);
  const intervalfaceMatchRef = useRef(null);

  const [imputadosReg, setImputadosReg] = useState();

  const [nombreImp, setNombreImp] = useState("");

  let labels;
  useEffect(() => {
    Axios.get("http://localhost:3000/consultarImputadosReg")
      .then((res) => {
        setImputadosReg(res.data);
        labels = res.data;
        console.log("Labels en efec: ", labels);
      })
      .catch((err) => {
        console.log("Hubo un error", err);
      })
      .finally(() => {
        main();
        console.log("Nombres", imputadosReg);
      });
  }, []);

  useEffect(() => {
    if (iniciarVideo) {
      startVideo();
    } else {
      stopVideo(stream);
    }
  }, [iniciarVideo]);

  useEffect(() => {
    if (nombreImp !== "") {
      const registrarAsistenciaImputado = () => {
        Axios.post("http://localhost:3000/registrarAsistenciaImputado", {
          nombreImp,
        })
          .then((res) => {
            onHide();
            stopModal(stream);
            window.location.reload();
          })
          .catch((err) => {
            console.log("Hubo un error", err);
          });
      };
      registrarAsistenciaImputado();
    }
  }, [nombreImp]);

  const registrarAsistenciaImputado = () => {
    Axios.post("http://localhost:3000/registrarAsistenciaImputado", {
      nombreImp,
    })
      .then((res) => {
        onHide();
        stopModal(stream);
      })
      .catch((err) => {
        console.log("Hubo un error", err);
      });
  };
  const getLabeledFaceDescriptions = () => {
    // const labels = ["Eduardo", "David"];
    // const imputados = []

    // const labels = ""
    console.log("En getLabeled: ", imputadosReg);
    return Promise.all(
      imputadosReg.map(async (label) => {
        const descriptions = [];
        for (let i = 0; i <= 2; i++) {
          console.log(label);
          const img = await faceapi.fetchImage(`/IMPUTADOS/${label}/${i}.jpg`);
          // const img = await faceapi.fetchImage(`../Users/epict/OneDrive/Documentos/Proyectos/Tauri_Trial/UMECA-SI-SERVER/IMPUTADOS/${label}/${i}.jpg`);
          const detections = await faceapi
            .detectSingleFace(img)
            .withFaceLandmarks()
            .withFaceDescriptor();
          descriptions.push(detections.descriptor);
        }
        console.log(descriptions);
        return new faceapi.LabeledFaceDescriptors(label, descriptions);
      })
    );
  };

  const faceMatch = async () => {
    const canvas = canvasRef.current;
    const labeledFaceDescriptors = await getLabeledFaceDescriptions();

    // const labeledFaceDescriptorsMessi = await getLabaledFaceDescriptionsMessi();
    // const labeledFaceDescriptorsJSON = faceapi.LabeledFaceDescriptors.fromJSON(labeledFaceDescriptors[0])

    console.log("En face match: ", labeledFaceDescriptors);
    // console.log("En face match Messi: ", labeledFaceDescriptorsMessi);

    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors);

    const displaySize = {
      width: videoRef.current.width,
      height: videoRef.current.height,
    };

    let rostros = [];
    faceapi.matchDimensions(canvas, displaySize);

    intervalfaceMatchRef.current = setInterval(async () => {
      const detections = await faceapi
        .detectAllFaces(videoRef.current)
        .withFaceLandmarks()
        .withFaceDescriptors();

      const resizedDetections = faceapi.resizeResults(detections, displaySize);

      canvas
        .getContext("2d", { willReadFrequently: true })
        .clearRect(0, 0, canvas.width, canvas.height);

      const results = resizedDetections.map((d) => {
        return faceMatcher.findBestMatch(d.descriptor);
      });

      results.forEach((result) => {
        console.log(result._label);
        if (result._label !== "unknown") {
          rostros.push(result._label);
        }
      });

      console.log("rostros imp capturado: ", rostros);

      //$ Para dibujar deteccion facial en pantalla
      // results.forEach((result, i) => {
      //   const box = resizedDetections[i].detection.box;
      //   const drawBox = new faceapi.draw.DrawBox(box, {
      //     label: result,
      //   });
      //   drawBox.draw(canvas);
      // });

      const stream = videoRef.current.srcObject;
      if (rostros.length === 10 && stream) {
        console.log("abajo interval:", stream);
        //#Cambie variable usuario para asegurar que yo soy el empleado logueado
        const nombreUsuario = nombreMasRepetido(rostros);

        console.log("Nombre mas repetido: ", nombreUsuario);
        setNombreImp(nombreUsuario);
        // const nombreUsuario = "Eduardo";
        // propsBoot.face(nombreUsuario);
        // stopVideo(stream);

        // registrarAsistenciaImputado();
      }
    }, 100);
  };

  function nombreMasRepetido(rostros) {
    let contador = {};
    let nombreMasRepetido = "";
    let maxRepeticiones = 0;

    for (let nombre of rostros) {
      contador[nombre] = (contador[nombre] || 0) + 1;
      if (contador[nombre] > maxRepeticiones) {
        maxRepeticiones = contador[nombre];
        nombreMasRepetido = nombre;
      }
    }

    return nombreMasRepetido;
  }

  const startVideo = async () => {
    try {
      const mediaStream = await navigator.mediaDevices.getUserMedia({
        video: true,
      });

      videoRef.current.srcObject = mediaStream;
      setStream(mediaStream);
      videoRef.current.onloadedmetadata = () => {
        faceMatch();
      };
    } catch (err) {
      console.error("Error al acceder a la webcam:", err);
    }
  };

  const stopVideo = (stream) => {
    if (stream) {
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoRef.current.srcObject = null;
      setStream(null);
    } else {
      return;
    }
  };

  const loadModels = async () => {
    await faceapi.nets.ssdMobilenetv1.load(modelPath);
    await faceapi.nets.ageGenderNet.load(modelPath);
    await faceapi.nets.faceLandmark68Net.load(modelPath);
    await faceapi.nets.faceRecognitionNet.load(modelPath);
    await faceapi.nets.faceExpressionNet.load(modelPath);
    await faceapi.nets.tinyFaceDetector.load(modelPath);
    // optionsSSDMobileNet = new faceapi.SsdMobilenetv1Options({
    //   minConfidence: minScore,
    //   maxResults,
    // });
  };

  const main = async () => {
    await faceapi.tf.setBackend("webgl");
    await faceapi.tf.ready();
    await loadModels();
    // await startVideo();
  };

  const stopModal = (stream) => {
    clearInterval(intervalfaceMatchRef.current);
    stopVideo(stream);
  };

  return (
    <>
      <style>
        {`
        .body-faceRecon{
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .canvas{
          position:absolute;
        }
        `}
      </style>
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <div className="webcam-impu">
            <Webcam
              audio={false}
              height={460}
              ref={webcamRef}
              screenshotFormat="image/jpg"
              width={620}
              videoConstraints={videoConstraints}
            />
          </div> */}
          <div className="body-faceRecon">
            <canvas ref={canvasRef} className="canvas"></canvas>
            <video
              ref={videoRef}
              id="video"
              width="620"
              height="460"
              autoPlay
              muted
              className="reconUser"
            ></video>
          </div>
        </Modal.Body>
        <Modal.Footer>
          {/* <button
            onClick={() => {
              setCargando(true);
              capture();
            }}
            disabled={cargando}
          >
            Capturar rostros
          </button> */}
          <button
            onClick={() => {
              // stopVideo(stream);
              setIniciarVideo(false);
              onHide();
            }}
          >
            Cerrar
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalAsistencia;
