import React, { useRef, useEffect, useState } from "react";
import * as faceapi from "@vladmandic/face-api";
import "./style/faceRecon.css";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useNavigate } from "react-router-dom";

function FaceRecon({ propsBoot }) {
  const [stream, setStream] = useState(null);
  const [intervals, setIntervals] = useState(true);
  const [rostroValido, setRostroValido] = useState(false);
  const videoRef = useRef(null);
  const canvasRef = useRef(null);
  const intervalfaceMatchRef = useRef(null);

  const path = "/assets/lib/face-api/models";
  const navegar = useNavigate();

  let optionsSSDMobileNet;

  const irMenu = (valido) => {
    navegar("/Menu");
  };

  const startVideo = async () => {
    try {
      const mediaStream = await navigator.mediaDevices.getUserMedia({
        video: true,
      });
      videoRef.current.srcObject = mediaStream;
      setStream(mediaStream);
      videoRef.current.onloadedmetadata = () => {
        loadModels();
      };
    } catch (err) {
      console.error("Error al acceder a la webcam:", err);
    }
  };

  const stopVideo = (stream) => {
    if (stream) {
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoRef.current.srcObject = null;
      setStream(null);
    } else {
      console.warn("Camara no detectada");
    }
  };

  const loadModels = async () => {
    Promise.all([
      faceapi.nets.faceLandmark68Net.loadFromUri("/assets/lib/face-api/models"),
      faceapi.nets.faceRecognitionNet.loadFromUri(
        "/assets/lib/face-api/models"
      ),
      faceapi.nets.ssdMobilenetv1.loadFromUri("/assets/lib/face-api/models"),
    ]).then(() => {
      faceMatch();
    });

    // await faceapi.nets.ssdMobilenetv1.load(path);
    // // await faceapi.nets.ageGenderNet.load(path);
    // await faceapi.nets.faceLandmark68Net.load(path);
    // await faceapi.nets.faceRecognitionNet.load(path);
    // // await faceapi.nets.faceExpressionNet.load(path);
    // optionsSSDMobileNet = new faceapi.SsdMobilenetv1Options({
    //   minConfidence: minScore,
    //   maxResults,
    // });
    // // faceDetection();
    // faceMatch()
  };

  const detectVideo = (video, canvas) => {};

  const faceDetection = () => {
    const canvas = canvasRef.current;

    const displaySize = {
      width: videoRef.current.width,
      height: videoRef.current.height,
    };

    faceapi.matchDimensions(canvas, displaySize);

    setInterval(async () => {
      const detections = await faceapi
        .detectAllFaces(videoRef.current, new faceapi.TinyFaceDetectorOptions())
        .withFaceLandmarks();

      const resizedDetections = faceapi.resizeResults(detections, displaySize);

      canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
      faceapi.draw.drawDetections(canvas, resizedDetections);
      faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
    }, 100);
  };

  const getLabeledFaceDescriptions = () => {
    const labels = ["Eduardo", "Messi"];
    return Promise.all(
      labels.map(async (label) => {
        const descriptions = [];
        for (let i = 1; i <= 2; i++) {
          const img = await faceapi.fetchImage(`/labels/${label}/${i}.jpg`);
          const detections = await faceapi
            .detectSingleFace(img)
            .withFaceLandmarks()
            .withFaceDescriptor();
          descriptions.push(detections.descriptor);
        }
        return new faceapi.LabeledFaceDescriptors(label, descriptions);
      })
    );
  };

  const storeFaceDescriptors = async () => {
    const labeledFaceDescriptors = await getLabeledFaceDescriptions();
    let getLabaled = [];
    getLabaled.push(labeledFaceDescriptors);

    const rostrosAnalizados = JSON.stringify(getLabaled);
    const nombreArchivo = "rostros.json";

    // console.log(getLabaled);
  };

  const faceMatch = async () => {
    const canvas = canvasRef.current;

    const labeledFaceDescriptors = await getLabeledFaceDescriptions();

    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors);

    const displaySize = {
      width: videoRef.current.width,
      height: videoRef.current.height,
    };

    let rostros = [];
    faceapi.matchDimensions(canvas, displaySize);

    intervalfaceMatchRef.current = setInterval(async () => {
      const detections = await faceapi
        .detectAllFaces(videoRef.current, new faceapi.TinyFaceDetectorOptions())
        .withFaceLandmarks()
        .withFaceDescriptors();

      const resizedDetections = faceapi.resizeResults(detections, displaySize);

      canvas
        .getContext("2d", { willReadFrequently: true })
        .clearRect(0, 0, canvas.width, canvas.height);

      const results = resizedDetections.map((d) => {
        return faceMatcher.findBestMatch(d.descriptor);
      });

      // console.log(rostros);

      results.forEach((result) => {
        console.log(result._label);
        if (result._label !== "unknown") {
          rostros.push(result._label);
        }
      });

      results.forEach((result, i) => {
        const box = resizedDetections[i].detection.box;
        const drawBox = new faceapi.draw.DrawBox(box, {
          label: result,
        });
        drawBox.draw(canvas);
      });

      // const stream = videoRef.current.srcObject;
      // if (rostros.length === 10 && stream) {
      //   console.log("abajo interval:", stream);
      //   const nombreUsuario = nombre(rostros);
      //   propsBoot.face(nombreUsuario);
      //   propsBoot.onHide();
      //   stopModal(stream);
      // }
    }, 100);

    function nombre(rostros) {
      let contador = {};
      let nombreMasRepetido = "";
      let maxRepeticiones = 0;

      for (let nombre of rostros) {
        contador[nombre] = (contador[nombre] || 0) + 1;
        if (contador[nombre] > maxRepeticiones) {
          maxRepeticiones = contador[nombre];
          nombreMasRepetido = nombre;
        }
      }

      return nombreMasRepetido;
    }
    // console.log(stream);
    // if (rostros.length === 15) {
    //   console.log(rostros);
    //   clearInterval(intervalfaceMatchRef.current);
    //   propsBoot.face("Lalo");
    // }
  };

  const stopModal = (stream) => {
    clearInterval(intervalfaceMatchRef.current);
    stopVideo(stream);
  };

  const closeButton = () => {
    clearInterval(intervalfaceMatchRef.current);
    stopVideo(stream);
    propsBoot.onHide();
  };

  return (
    <>
      <Modal.Body>
        <div className="body-faceRecon">
          <canvas ref={canvasRef} className="canvas-reconUser"></canvas>
          <video
            ref={videoRef}
            id="video"
            width="620"
            height="460"
            autoPlay
            muted
            className="reconUser"
          ></video>
        </div>
      </Modal.Body>

      <Modal.Footer>
        <button onClick={startVideo}>Verificar</button>
        <button
          onClick={() => {
            stopVideo(stream);
          }}
        >
          Detener
        </button>
        <Button onClick={closeButton}>Close</Button>
      </Modal.Footer>
    </>
  );
}

export default FaceRecon;
