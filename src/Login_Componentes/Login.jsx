import React, { useState, useEffect } from "react";
// import "./style/login.css";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";
import Header from "../Header";
import ModalLogin from "./ModalLogin";
import ModalLoginUser from "./ModalLoginUser";

/**
 * Este componente es para crear nuestro login, desde el diseño hasta su logica.
 */

function Login({ estado }) {
  const [modalShow, setModalShow] = useState(false);
  const [modalShowUser, setModalShowUser] = useState(false);
  const [autentificado, setAutentificado] = useState(true);
  const [faceName, setFaceName] = useState("");
  const navegar = useNavigate();

  useEffect(() => {
    window.addEventListener("beforeunload", antesDeCargar);

    return () => {
      window.removeEventListener("beforeunload", antesDeCargar);
    };
  }, []);

  const antesDeCargar = () => {
    localStorage.clear();
  };

  const nombreUsuario = (nombre) => {
    /**
     * Realizar aqui peticion a la base de datos para verificar si el usuario existe
     */
    // if (nombre === "1") {
    //   console.log("En nombre usuario: ", nombre);
    //   localStorage.setItem("token", autentificado);
    //   estado(autentificado);
    //   setFaceName(nombre);
    //   setModalShow(false);
    //   setModalShowUser(true);
    // } else {
    //   setModalShow(false);
    // }

    setFaceName(nombre);
    setModalShow(false);
    setModalShowUser(true);
  };

  return (
    <>
      <style type="text/css">
        {`
.body {
  // background-image: url("/login.jpeg");
  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
  font-family: Arial, sans-serif;
}


#encabezado {
  background-color: #afafaf;
  color: rgb(0, 0, 0);
  padding: 20px;
  text-align: right;
  border-radius: 0 0 10px 10px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
}

.container {
  max-width: 500px;
  background-color: rgba(219, 214, 214, 0.8); /* Fondo borroso */
  border-radius: 10px;
  padding: 30px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  margin: 50px auto;
}

.container h2 {
  text-align: center;
  margin-bottom: 80px;
}
.form-control {
  border-radius: 20px;
}

.btn-primary {
  border-radius: 20px;
  width: 100%;
  background-color: #4e4e4e;
  color: white;
  font-family: "Arial", sans-serif;
  font-size: 16px;
}

.btn-primary:hover {
  background-color: #9ba79e;
}

.text-center {
  text-align: center;
}

.canvas-reconUser{
  position:absolute;
}

`}
      </style>
      <div className="body">
        <Header/>
        <div className="container mt-5">
          <h2>Iniciar Sesión</h2>
          <form action="#" method="POST">
            {/* <div className="mb-3">
              <input
                type="email"
                className="form-control"
                placeholder="Correo electrónico"
                id="correo"
                name="correo"
                required
              />
            </div> */}

            {/* <div className="mb-3">
              <input
                type="password"
                className="form-control"
                placeholder="Contraseña"
                id="password"
                name="password"
                required
              />
            </div> */}

            <button
              type="submit"
              className="btn btn-primary"
              onClick={(e) => {
                setModalShow(true);
                e.preventDefault();
                // if (autentificado) {
                //   localStorage.setItem("token", autentificado);
                //   estado(autentificado);
                //   navegar("/Menu");
                // }
              }}
            >
              Ingresar
            </button>

            <ModalLogin
              show={modalShow}
              onHide={() => setModalShow(false)}
              face={nombreUsuario}
            />

            <ModalLoginUser
              show={modalShowUser}
              onHide={() => setModalShowUser(false)}
              nombre={faceName}
              estado={estado}
            />

            {/* <p className="text-center mt-3">
              <a
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#olvidarContrasenaModal"
              >
                ¿Olvidaste tu contraseña?
              </a>
            </p>
            <p className="text-center mt-3">
              ¿No tienes una cuenta? <a href="registro.html">Regístrate aquí</a>
            </p> */}
          </form>
        </div>

        <div
          className="modal fade"
          id="olvidarContrasenaModal"
          tabIndex="-1"
          aria-labelledby="olvidarContrasenaModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="olvidarContrasenaModalLabel">
                  ¿Olvidaste tu contraseña?
                </h5>

                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label htmlFor="correoOlvidado" className="form-label">
                      Correo electrónico:
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="correoOlvidado"
                      required
                    />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Enviar
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
