import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import InputPassword from "./InputPassword";
import Axios from "axios";
/**
 * Modal que se despliega cuando el nombre del rostro es obtenido
 * Aqui se introducira la contraseña y se validara posteriormente el usuario.
 */

function ModalLoginUser(props) {
  const [autentificado, setAutentificado] = useState(true);
  const { show, nombre, onHide, estado } = props;
  const navegar = useNavigate();

  const [password, setPassword] = useState("");

  const ingresar = () => {
    /**
     * Aqui hay que hacer la peticion de login a la base de datos
     */

    Axios.post("http://localhost:3000/login", {
      nombre,
      password,
    })
      .then(() => {
        onHide();
        localStorage.setItem("token", autentificado);
        estado(autentificado);
        navegar("/Menu");
      })
      .catch(() => {
        onHide();
        window.location.reload();
      });
  };

  const closeModalUser = () => {
    props.onHide();
  };
  return (
    <>
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            {/* Hola {nombre}! */}
            Bienvenido!
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputPassword place="Contraseña" setCampo={setPassword} />
        </Modal.Body>
        <Modal.Footer>
          <button onClick={ingresar}>Ingresar</button>
          <button onClick={closeModalUser}>Cerrar</button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalLoginUser;
