import React from "react";
function InputPassword({ place, setCampo }) {
  return (
    <>
      <input
        type="password"
        required
        autoComplete="off"
        placeholder={place}
        // className="form-control"
        onChange={(e) => {
          setCampo(e.target.value);
        }}
      />
    </>
  );
}


export default InputPassword;
