import React from "react";
import { useRef, useEffect, useState } from "react";
import * as faceapi from "@vladmandic/face-api";
import "../style/faceReconFixed.css"
import Axios from "axios";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

/**
 *
 * Este es el componente mas importante, almacena todos nuestros scripts para:
 * 1.- Encender nuestra webcam
 * 2.- Cargar los modelos de la api
 * 3.- Detectar rostros
 * 4.- Determinar quien seria el usuario dependiendo el rostro detectado.
 */

function FaceReconFixed({ propsBoot }) {
  const modelPath = "/assets/lib/face-api/models";
  const minScore = 0.5;
  const maxResults = 100;
  let optionsSSDMobileNet;

  const videoRef = useRef(null);
  const canvasRef = useRef(null);
  const intervalfaceMatchRef = useRef(null);

  const [stream, setStream] = useState(null);

  const [empleadosReg, setEmpleadosReg] = useState();

  const [nombreEmp, setnombreEmp] = useState("")

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarEmpleadosReg")
      .then((res) => {
        setEmpleadosReg(res.data);
      })
      .catch((err) => {
        console.log("Hubo un error", err);
      })
      .finally(() => {
        main();
      });
  }, []);

  // const dibujarCaritas = () => {
  //   const ctx = canvasRef.getContext("2d", { willReadFrequently: true });
  // };

  // const detectarVideo = () => {
  //   const canvas = canvasRef.current;
  //   if (!videoRef || videoRef.paused) return false;
  //   const t0 = performance.now();
  //   console.log("Se detecto video", t0);
  //   // console.log(optionsSSDMobileNet);

  //   const displaySize = {
  //     width: videoRef.current.width,
  //     height: videoRef.current.height,
  //   };

  //   faceapi.matchDimensions(canvas, displaySize);

  //   intervalfaceMatchRef.current = setInterval(async () => {
  //     const detections = await faceapi
  //       .detectAllFaces(videoRef.current, optionsSSDMobileNet)
  //       .withFaceLandmarks()
  //       .withFaceDescriptors();

  //     const resizedDetections = faceapi.resizeResults(detections, displaySize);

  //     canvas
  //       .getContext("2d", { willReadFrequently: true })
  //       .clearRect(0, 0, canvas.width, canvas.height);
  //     faceapi.draw.drawDetections(canvas, resizedDetections);
  //     faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
  //   }, 100);
  // };

  const getLabeledFaceDescriptions = () => {
    // const labels = ["Eduardo", "David"];
    // const imputados = []

    console.log("En getLabeled: ", empleadosReg);
    return Promise.all(
      empleadosReg.map(async (label) => {
        const descriptions = [];
        for (let i = 0; i <= 2; i++) {
          const img = await faceapi.fetchImage(`./EMPLEADOS/${label}/${i}.jpg`);
          // console.log(img);
          const detections = await faceapi
            .detectSingleFace(img)
            .withFaceLandmarks()
            .withFaceDescriptor();
          descriptions.push(detections.descriptor);
        }
        console.log(descriptions);
        return new faceapi.LabeledFaceDescriptors(label, descriptions);
      })
    );
  };

  // const descripcionesRostros = async () => {
  //   // console.log("Descripciones", await getLabeledFaceDescriptions());

  //   const url = "http://localhost:3000/rostros";

  //   // console.log("descripciones bruto: ",descripciones);
  //   // const descripcionesJSON = JSON.stringify(descripciones)
  //   // console.log("descripciones JSON", descripcionesJSON);

  //   const descripciones = await getLabeledFaceDescriptions();
  //   // console.log("descripciones to json: ", descripciones);

  //   // const descLabeled = faceapi.LabeledFaceDescriptors.fromJSON(descripciones);
  //   const descLabeled = faceapi.LabeledFaceDescriptors.fromJSON(
  //     descripciones[0]
  //   );
  //   console.log("descLabeled: ", descLabeled);
  //   Axios.post(url, descripciones)
  //     .then((res) => {
  //       console.log("respuesta", res);
  //     })
  //     .catch((err) => {
  //       console.error("error: ", err);
  //     });
  // };

  // const fromJSON = (json) => {
  //   console.log("En fromjson: ", json);
  //   const descriptors = json.descriptors.map((d) => new Float32Array(d));
  //   return new faceapi.LabeledFaceDescriptors(json.label, descriptors);
  // };

  const faceMatch = async () => {
    const canvas = canvasRef.current;
    const labeledFaceDescriptors = await getLabeledFaceDescriptions();

    // const labeledFaceDescriptorsMessi = await getLabaledFaceDescriptionsMessi();
    // const labeledFaceDescriptorsJSON = faceapi.LabeledFaceDescriptors.fromJSON(labeledFaceDescriptors[0])

    console.log("En face match: ", labeledFaceDescriptors);
    // console.log("En face match Messi: ", labeledFaceDescriptorsMessi);

    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors);

    const displaySize = {
      width: videoRef.current.width,
      height: videoRef.current.height,
    };

    let rostros = [];
    faceapi.matchDimensions(canvas, displaySize);

    intervalfaceMatchRef.current = setInterval(async () => {
      const detections = await faceapi
        .detectAllFaces(videoRef.current)
        .withFaceLandmarks()
        .withFaceDescriptors();

      const resizedDetections = faceapi.resizeResults(detections, displaySize);

      canvas
        .getContext("2d", { willReadFrequently: true })
        .clearRect(0, 0, canvas.width, canvas.height);

      const results = resizedDetections.map((d) => {
        return faceMatcher.findBestMatch(d.descriptor);
      });

      results.forEach((result) => {
        console.log(result._label);
        if (result._label !== "unknown") {
          rostros.push(result._label);
        }
      });

      console.log(rostros);

      //$ Para dibujar deteccion facial en pantalla
      // results.forEach((result, i) => {
      //   const box = resizedDetections[i].detection.box;
      //   const drawBox = new faceapi.draw.DrawBox(box, {
      //     label: result,
      //   });
      //   drawBox.draw(canvas);
      // });

      const stream = videoRef.current.srcObject;
      if (rostros.length === 10 && stream) {
        console.log("abajo interval:", stream);
        //#Cambie variable usuario para asegurar que yo soy el empleado logueado
        const nombreUsuario = nombre(rostros);
        // const nombreUsuario = "Eduardo";
        setEmpleadosReg(nombreUsuario)
        propsBoot.face(nombreUsuario);
        propsBoot.onHide();
        stopModal(stream);
      }
    }, 100);
  };

  function nombre(rostros) {
    let contador = {};
    let nombreMasRepetido = "";
    let maxRepeticiones = 0;

    for (let nombre of rostros) {
      contador[nombre] = (contador[nombre] || 0) + 1;
      if (contador[nombre] > maxRepeticiones) {
        maxRepeticiones = contador[nombre];
        nombreMasRepetido = nombre;
      }
    }

    return nombreMasRepetido;
  }

  const startVideo = async () => {
    try {
      const mediaStream = await navigator.mediaDevices.getUserMedia({
        video: true,
      });

      videoRef.current.srcObject = mediaStream;
      setStream(mediaStream);
      videoRef.current.onloadedmetadata = () => {
        faceMatch();
      };
    } catch (err) {
      console.error("Error al acceder a la webcam:", err);
    }
  };

  const stopVideo = (stream) => {
    if (stream) {
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoRef.current.srcObject = null;
      setStream(null);
    } else {
      return;
    }
  };

  const loadModels = async () => {
    await faceapi.nets.ssdMobilenetv1.load(modelPath);
    await faceapi.nets.ageGenderNet.load(modelPath);
    await faceapi.nets.faceLandmark68Net.load(modelPath);
    await faceapi.nets.faceRecognitionNet.load(modelPath);
    await faceapi.nets.faceExpressionNet.load(modelPath);
    await faceapi.nets.tinyFaceDetector.load(modelPath);
    // optionsSSDMobileNet = new faceapi.SsdMobilenetv1Options({
    //   minConfidence: minScore,
    //   maxResults,
    // });
    console.log("Modelos cargados");
  };

  const main = async () => {
    await faceapi.tf.setBackend("webgl");
    await faceapi.tf.ready();
    await loadModels();
  };

  const stopModal = (stream) => {
    clearInterval(intervalfaceMatchRef.current);
    stopVideo(stream);
  };

  const closeButton = () => {
    clearInterval(intervalfaceMatchRef.current);
    stopVideo(stream);
    propsBoot.onHide();
  };

  return (
    <>
      <Modal.Body>
        <div className="body-faceRecon">
          <canvas ref={canvasRef} className="canvas-reconUser"></canvas>
          <video
            ref={videoRef}
            id="video"
            width="620"
            height="460"
            autoPlay
            muted
            className="reconUser"
          ></video>
        </div>
      </Modal.Body>

      <Modal.Footer>
        <button onClick={startVideo}>Verificar</button>
        <button
          onClick={() => {
            stopVideo(stream);
          }}
        >
          Detener
        </button>
        <Button onClick={closeButton}>Close</Button>
      </Modal.Footer>
    </>
  );
}

export default FaceReconFixed;
