import Modal from "react-bootstrap/Modal";
// import FaceRecon from "./FaceRecon";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import FaceReconFixed from "./FaceReconFixed";

/**
 * Componente para crear nuestro modal para iniciar sesion,
 * mostrara el video de nuestra webcam.
 */

function ModalLogin(props) {
  const { show, face } = props;
  return (
    <>
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Verificar identidad
          </Modal.Title>
        </Modal.Header>

        <FaceReconFixed propsBoot={props} face={face}/>
      </Modal>
    </>
  );
}

export default ModalLogin;
