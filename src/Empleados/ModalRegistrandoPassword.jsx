import React from "react";
import Modal from "react-bootstrap/Modal";
import RegistrandoContraseña from "./RegistrandoContraseña";
function ModalRegistrandoPassword({
  show,
  onHide,
  setPassword,
  agregarEmpleado,
}) {
  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header>
          <Modal.Title>Registra tu contraseña</Modal.Title>
        </Modal.Header>
        {/* <RegistrandoContraseña place="Contraseña" setPassword={setPassword} onHide={onHide} agregarEmpleado={agregarEmpleado}/> */}

        <Modal.Body>
          <input
            type="password"
            required
            placeholder="Contraseña"
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(e.target.value);
            }}
          />
        </Modal.Body>

        <Modal.Footer>
          <button
            onClick={() => {
              agregarEmpleado();
            }}
          >
            Registrar
          </button>
          {/* <button
            onClick={() => {
              agregarEmpleado();
            }}
          >
            Registrar
          </button> */}
          <button
            onClick={() => {
              onHide();
              window.location.reload();
            }}
          >
            Cancelar
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalRegistrandoPassword;
