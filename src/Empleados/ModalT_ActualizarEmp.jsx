import React from "react";
import Modal from "react-bootstrap/Modal";
import { useState, useEffect } from "react";
import Axios from "axios";
import InputActualizarEmp from "../Inputs/InputActualizarEmp";

import InputActualizarEdad from "../Inputs/formulario_validaciones_update/InputActualizarEdad";
import InputActualizarTel from "../Inputs/formulario_validaciones_update/InputActualizarTel";
import InputActualizarRFC from "../Inputs/formulario_validaciones_update/InputActualizarRFC";
import InputActualizarCURP from "../Inputs/formulario_validaciones_update/InputActualizarCURP";
import InputActualizarCargo from "../Inputs/formulario_validaciones_update/InputActualizarCargo";
function ModalT_ActualizarEmp({ show, onHide, empleadosData }) {
  const [nombre, setNombre] = useState("");
  const [apellidoPaterno, setApellidoPaterno] = useState("");
  const [apellidoMaterno, setApellidoMaterno] = useState("");
  const [curp, setCurp] = useState("");
  const [edad, setEdad] = useState("");
  const [telefono, setTelefono] = useState("");
  const [rfc, setRfc] = useState("");
  const [cargo, setCargo] = useState("");

  const actualizarEmpleado = () => {
    Axios.post("http://localhost:3000/ActualizarEmpleado", {
      id_empleado: empleadosData.id_empleado,
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      curp,
      edad,
      telefono,
      rfc,
      cargo,
    })
      .then((res) => {
        onHide();
      })
      .catch((err) => {
        console.log("Hubo un error en actualizar empleado", err);
      });
  };

  return (
    <>
      <Modal
        show={show}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title>Actualizar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="col-md-9 cont2">
              <div className="formulario">
                {/* <h2>Ingregar Imputado</h2> */}
                <form onSubmit={actualizarEmpleado}>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarEmp
                          setCampo={setNombre}
                          place={empleadosData.nombre}
                          nombre="Nombre: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarEmp
                          setCampo={setApellidoPaterno}
                          place={empleadosData.apellidoPaterno}
                          nombre="Apellido Paterno: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarEmp
                          setCampo={setApellidoMaterno}
                          place={empleadosData.apellidoMaterno}
                          nombre="Apellido Materno: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <InputActualizarCURP
                        setCampo={setCurp}
                        place={empleadosData.curp}
                        nombre="CURP: "
                      />
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarEdad
                          setCampo={setEdad}
                          place={empleadosData.edad}
                          nombre="Edad: "
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarTel
                          setCampo={setTelefono}
                          place={empleadosData.telefono}
                          nombre="Teléfono: "
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarRFC
                          setCampo={setRfc}
                          place={empleadosData.rfc}
                          nombre="RFC: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarCargo
                          setCampo={setCargo}
                          place={empleadosData.cargo}
                          campo={cargo}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="botones">
                    <button
                      type="submit"
                      className="btn btn-secondary"
                      onClick={() => {
                        actualizarEmpleado();
                      }}
                    >
                      Actualizar
                    </button>
                    <button
                      type="submit"
                      className="btn btn-secondary"
                      onClick={(e) => {
                        e.preventDefault();
                        onHide();
                      }}
                    >
                      Cerrar
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalT_ActualizarEmp;
