import React from "react";
import FormularioEmpleados from "./FormularioEmpleados";
import Header from "../Header";
  
function Empleados() {
  return (
    <>
      <Header encabezado="EMPLEADOS"/>
      <FormularioEmpleados />
    </>
  );
}

export default Empleados;
