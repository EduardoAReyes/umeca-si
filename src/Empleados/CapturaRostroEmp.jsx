import React, { useRef, useState } from "react";
import Webcam from "react-webcam";
import Modal from "react-bootstrap/Modal";
import Axios from "axios";

const videoConstraints = {
  width: 620,
  height: 460,
  facingMode: "user",
};
function CapturaRostroEmp({ onHide, nombreEmpleado, agregarEmpleado }) {
  const webcamRef = useRef();
  const [cargando, setCargando] = useState(false);

  let rostros = [];

  const capture = React.useCallback(() => {
    if (cargando) return;

    for (let i = 0; i < 3; i++) {
      const imageSrc = webcamRef.current.getScreenshot();
      rostros.push(imageSrc);
      console.log("rostros en capture: ", rostros);
    }
    // setWebUrl(imageSrc);
    console.log(nombreEmpleado);
    Axios.post("http://localhost:3000/capturarRostroEmp", {
      imagenes: rostros,
      nombre: nombreEmpleado,
    })
      .then((res) => {
        rostros = [];
        agregarEmpleado();
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setCargando(false);
      });
  }, [webcamRef]);
  return (
    <>
      <style>{`
    .webcam-impu{
      margin: 0;
      padding: 0;
      display: flex;
      justify-content: center;
      align-items: center;
  }
    `}</style>
      <Modal.Body>
        <div className="webcam-impu">
          <Webcam
            audio={false}
            height={460}
            ref={webcamRef}
            screenshotFormat="image/jpg"
            width={620}
            videoConstraints={videoConstraints}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          onClick={() => {
            setCargando(true);
            capture();
          }}
          disabled={cargando}
        >
          Capturar rostros
        </button>
        <button
          onClick={() => {
            onHide();
            window.location.reload();
          }}
        >
          Cerrar
        </button>
      </Modal.Footer>
    </>
  );
}

export default CapturaRostroEmp;
