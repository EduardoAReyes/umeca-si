import React from "react";
import { useState } from "react";
import InputEmpleados from "../Inputs/InputsEmpleados";
import Axios from "axios";
import ModalT_Empleados from "./ModalT_Empleados";
import ModalT_ActualizarEmp from "./ModalT_ActualizarEmp";
import { useNavigate, Link } from "react-router-dom";
// import ModalRegistrandoPassword from "../Modals/ModalRegistrandoPassword";
import ModalRegistrandoPassword from "./ModalRegistrandoPassword";
import ModalCapturaRostroEmp from "./ModalCapturaRostroEmp";

import InputTelefono from "../Inputs/formulario_validaciones/InputTelefono";
import InputRFC from "../Inputs/formulario_validaciones/InputRFC";
import InputEdad from "../Inputs/formulario_validaciones/InputEdad";
import InputCargo from "../Inputs/formulario_validaciones/InputCargo";
import InputCURP from "../Inputs/formulario_validaciones/InputCURP";
function FormularioEmpleados() {
  const navegar = useNavigate();

  const [nombre, setNombre] = useState("");
  const [apellidoPaterno, setApellidoPaterno] = useState("");
  const [apellidoMaterno, setApellidoMaterno] = useState("");
  const [curp, setCurp] = useState("");
  const [edad, setEdad] = useState("");
  const [telefono, setTelefono] = useState("");
  const [rfc, setRfc] = useState("");
  const [cargo, setCargo] = useState("");
  const [password, setPassword] = useState("");

  const [modalT_EmpleadoShow, setModalT_EmpleadoShow] = useState(false);

  const [empleadosData, setEmpleadosData] = useState("");

  const [ModalT_ActualizarEmpShow, setModalT_ActualizarEmpShow] =
    useState(false);

  const [ModalCapturaRostroEmpShow, setModalCapturaRostroEmpShow] =
    useState(false);

  const [ModalRegistrandoContraseña, setModalRegistrandoContraseña] =
    useState(false);

  const agregarEmpleado = () => {
    Axios.post("http://localhost:3000/agregarEmpleado", {
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      curp,
      edad,
      telefono,
      rfc,
      cargo,
      password,
    })
      .then((res) => {
        setModalCapturaRostroEmpShow(false);
        window.location.reload();
      })
      .catch((err) => {
        console.log("Hubo un error en agregar empleado", err);
      });
  };

  const capturarRostro = () => {
    if (
      nombre !== "" ||
      apellidoPaterno !== "" ||
      curp !== "" ||
      edad !== "" ||
      telefono !== "" ||
      rfc !== "" ||
      cargo !== "" ||
      password !== ""
    ) {
      setModalCapturaRostroEmpShow(true);
      console.log(curp);
    } else {
      return;
    }
  };

  const registrarContraseña = () => {
    setModalRegistrandoContraseña(true);
  };

  return (
    <>
      <style>{`
    .container{
      display:flex;
      justify-content:center;
      align-items:center;
    }
    .formulario {
      background-color: #fff;
      border-radius: 10px;
      padding: 20px;
      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  }

  .form-group {
      margin-bottom: 20px;
  }

  label {
      font-weight: bold;
  }

  .flecha-a-menu{
    position:fixed;
    width:50px;
  }

  .botones{
    display: flex;
    justify-content: space-around;
    align-items: center;
}

    `}</style>
      <Link to="/Menu">
        <img src="arrow.svg" className="flecha-a-menu" />
      </Link>
      <ModalT_Empleados
        show={modalT_EmpleadoShow}
        onHide={() => setModalT_EmpleadoShow(false)}
        setEmpleadosData={setEmpleadosData}
        setModalActualizarEmp={setModalT_ActualizarEmpShow}
      />

      <ModalT_ActualizarEmp
        show={ModalT_ActualizarEmpShow}
        onHide={() => setModalT_ActualizarEmpShow(false)}
        empleadosData={empleadosData}
      />

      <ModalCapturaRostroEmp
        show={ModalCapturaRostroEmpShow}
        onHide={() => setModalCapturaRostroEmpShow(false)}
        nombreEmpleado={curp}
        agregarEmpleado={() => {
          agregarEmpleado();
        }}
      />

      <ModalRegistrandoPassword
        show={ModalRegistrandoContraseña}
        onHide={() => setModalRegistrandoContraseña(false)}
        setPassword={setPassword}
        // agregarEmpleado={() => {
        //   agregarEmpleado();
        // }}
        agregarEmpleado={() => {
          capturarRostro();
        }}
      />

      <div className="container">
        <div className="col-md-9 cont2">
          <div className="formulario">
            <h2>Agregar empleado</h2>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                registrarContraseña();
                // capturarRostro();
              }}
            >
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <InputEmpleados place="Nombre" setCampo={setNombre} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputEmpleados
                      place="Apellido Paterno"
                      setCampo={setApellidoPaterno}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputEmpleados
                      place="Apellido Materno"
                      setCampo={setApellidoMaterno}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputCURP place="CURP" setCampo={setCurp} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputEdad place="Edad" setCampo={setEdad} />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputTelefono setCampo={setTelefono} place="Teléfono" />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputRFC setCampo={setRfc} place="RFC" />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputCargo setCampo={setCargo} />
                  </div>
                </div>
              </div>
              <div className="botones">
                <button type="submit" className="btn btn-secondary">
                  Registrar
                </button>
                {/* <button
                  type="submit"
                  className="btn btn-secondary"
                  onClick={(e) => {
                    e.preventDefault();
                    capturarRostro();
                  }}
                >
                  Registrar
                </button> */}
                <button
                  type="submit"
                  className="btn btn-secondary"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalT_EmpleadoShow(true);
                  }}
                >
                  Tabla
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default FormularioEmpleados;
