import React from "react";
import Modal from "react-bootstrap/Modal";
import CapturaRostroEmp from "./CapturaRostroEmp";

function ModalCapturaRostroEmp({
  show,
  onHide,
  nombreEmpleado,
  agregarEmpleado,
}) {
  return (
    <>
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title>Capturar Empleado</Modal.Title>
        </Modal.Header>
        <CapturaRostroEmp
          onHide={onHide}
          nombreEmpleado={nombreEmpleado}
          agregarEmpleado={agregarEmpleado}
        />
      </Modal>
    </>
  );
}

export default ModalCapturaRostroEmp;
