import React from "react";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/table";
import { useState, useEffect } from "react";
import Axios from "axios";
function ModalT_Empleados({
  show,
  onHide,
  setEmpleadosData,
  setModalActualizarEmp,
}) {
  const [ultimoCampo, setUltimoCampo] = useState([]);
  const [cambio, setCambio] = useState(false);

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarEmpleados")
      .then((res) => {
        setUltimoCampo(res.data);
      })
      .catch((err) => {
        console.log("Hubo un error en consulta de empleados", err);
      });
  }, []);

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarEmpleados")
      .then((res) => {
        setUltimoCampo(res.data);
      })
      .catch((err) => {
        console.log("Hubo un error en consulta de empleados", err);
      })
      .finally(() => {
        setCambio(false);
      });
  }, [cambio]);

  const actualizarEmpleado = (
    id_empleado,
    nombre,
    apellidoPaterno,
    apellidoMaterno,
    curp,
    edad,
    telefono,
    rfc,
    cargo
  ) => {
    const data = {
      id_empleado,
      nombre,
      apellidoPaterno,
      apellidoMaterno,
      curp,
      edad,
      telefono,
      rfc,
      cargo,
    };
    setEmpleadosData(data);
  };

  const eliminarCapturasEmp = () => {
    Axios.post("/eliminarCapturasEmp", {})
      .then(() => {})
      .catch(() => {});
  };

  const eliminarEmpleado = (id_empleado, curp) => {
    Axios.post("http://localhost:3000/eliminarEmpleado", {
      id_empleado,
      curp,
    })
      .then((res) => {
        setCambio(true);
      })
      .catch((err) => {
        console.log("Hubo un error en eliminar empleado", err);
      });
  };

  return (
    <>
      <style>
        {`
      .tablaImp{
        max-height:300px;
        overflow-y:auto;
      }
      `}
      </style>
      <Modal
        show={show}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            EMPLEADOS
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="tablaImp">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Apellido Paterno</th>
                  <th>Apellido Materno</th>
                  <th>CURP</th>
                  <th>Edad</th>
                  <th>Teléfono</th>
                  <th>RFC</th>
                  <th>Cargo</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {ultimoCampo.map((val) => (
                  <tr key={val.id_empleado}>
                    <td>{val.id_empleado}</td>
                    <td>{val.nombre}</td>
                    <td>{val.apellido_Paterno}</td>
                    <td>{val.apellido_Materno}</td>
                    <td>{val.curp}</td>
                    <td>{val.edad}</td>
                    <td>{val.telefono}</td>
                    <td>{val.rfc}</td>
                    <td>{val.cargo}</td>
                    <td>
                      <button
                        type="button"
                        onClick={(e) => {
                          actualizarEmpleado(
                            val.id_empleado,
                            val.nombre,
                            val.apellido_Paterno,
                            val.apellido_Materno,
                            val.curp,
                            val.edad,
                            val.telefono,
                            val.rfc,
                            val.cargo
                          );
                          onHide();
                          setModalActualizarEmp(true);
                        }}
                      >
                        Actualizar
                      </button>
                    </td>
                    <td>
                      <button
                        type="button"
                        onClick={(e) => {
                          e.preventDefault();
                          eliminarEmpleado(val.id_empleado,val.curp);
                        }}
                      >
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => {
              onHide();
            }}
          >
            Cerrar
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalT_Empleados;
