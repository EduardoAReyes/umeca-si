import React from "react";

import Modal from "react-bootstrap/Modal";
function RegistrandoContraseña({
  place,
  setPassword,
  onHide,
  agregarEmpleado,
}) {
  return (
    <>
      <Modal.Body>
        <input
          type="password"
          required
          placeholder={place}
          onChange={(e) => {
            setPassword(e.target.value);
            console.log(e.target.value);
          }}
        />
      </Modal.Body>

      <Modal.Footer>
        <button
          onClick={(e) => {
            e.preventDefault()
            agregarEmpleado();
          }}
        >
          Registrar
        </button>
        <button
          onClick={() => {
            onHide();
          }}
        >
          Cancelar
        </button>
      </Modal.Footer>
    </>
  );
}

export default RegistrandoContraseña;
