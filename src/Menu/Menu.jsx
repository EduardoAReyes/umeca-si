import React from "react";
import { useNavigate } from "react-router-dom";
import Header from "../Header";
import { Link } from "react-router-dom";
import Imputados from "../Imputados/Imputados";
/**
 * Componente para implementar el menu del sistema,
 */

function Menu() {
  const navegar = useNavigate();
  return (
    <>
      <style type="text/css">
        {`
                body {
                    background-color: #f8f9fa;
                }

                .container {
                    max-width: 1200px;
                    margin: 20px auto;
                }

                .row {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                .col-left, .col-right {
                    flex: 1;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                .profile-pic {
                    width: 100px;
                    height: 100px;
                    border-radius: 50%;
                    margin-bottom: 20px;
                    border: 4px solid #007bff;
                    transition: transform 0.3s ease;
                }

                .profile-pic:hover {
                    transform: rotate(360deg);
                }

                a{
                  cursor:pointer;
                }

                .links {
                    text-align: center;
                    margin-bottom: 20px;
                    
                }

                .links a {
                    display: block;
                    margin-bottom: 10px;
                    font-weight: bold;
                    color: #007bff;
                    text-decoration: none;
                    transition: color 0.3s ease;
                }

                .links a i {
                    margin-right: 5px;
                }

                .links a:hover {
                    color: #0056b3;
                    text-decoration: none;
                    transform: scale(1.1);
                    transition: transform 0.3s ease;
                }

                .col-right img {
                    max-width: 200px;
                    height: auto;
                    border-radius: 10px;
                    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);
                }

                /* Estilos adicionales */
                .section-header {
                    border-bottom: 1px solid #dee2e6;
                    padding-bottom: 10px;
                    margin-bottom: 20px;
                }

                .title-menu {
                    font-size: 2rem;
                    color: #8d50ff;
                    font-weight: bold;
                    text-transform: uppercase;
                }

                .title-umeca {
                    font-size: 2rem;
                    color: #28a745;
                    font-weight: bold;
                    text-transform: uppercase;
                }

                @media (max-width: 768px) {
                    .row {
                        flex-direction: column;
                        text-align: center;
                    }

                    .col-left, .col-right {
                        text-align: center;
                        margin-bottom: 20px;
                    }
                }

                .flecha-a-raiz{
                        position:fixed;
                        width:50px;
                }
        `}
      </style>
      <Header encabezado="MENU" />

      <Link to="/">
        <img src="arrow.svg" className="flecha-a-raiz" />
      </Link>
      <div className="container">
        <div className="row">
          <div className="col-3 col-left"></div>
          <div className="col-6">
            <div className="links">
              <a
                onClick={() => {
                  navegar("/Imputados");
                }}
              >
                <i className="fas fa-users fa-lg">
                  <h1>Imputados</h1>
                </i>
              </a>
              <a
                onClick={() => {
                  navegar("/Sobreseguimiento");
                }}
              >
                <i className="fas fa-users fa-lg"><h1>Sobreseguimiento</h1></i>
              </a>
              <a
                onClick={() => {
                  navegar("/Empleados");
                }}
              >
                <i className="fas fa-users fa-lg"><h1>Empleados</h1></i>
              </a>
            </div>
          </div>
          <div className="col-3 col-right">
            {/* <img src="https://via.placeholder.com/200" alt="Imagen Derecha" /> */}
          </div>
        </div>
      </div>
      {/* <button
        onClick={() => {
          navegar("/");
        }}
      >
        Cerrar sesion
      </button> */}
    </>
  );
}

export default Menu;
