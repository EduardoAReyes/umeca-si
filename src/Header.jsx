import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";

function Header({ encabezado }) {
  return (
    <>
      <style>{`
      
    #encabezado {
      display:flex;
      justify-content: space-between;
      height:150px;
      align-items:center;
      background-color: #afafaf;
      color: rgb(0, 0, 0);
      padding: 20px;
      text-align: right;
      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .img-logo{
      width:150px;
      position:absolute;
      top:0;
      right:0;
    }
      `}</style>

      <div id="encabezado">
        <h1 className="titulo">{encabezado}</h1>
        <div className="der">
          <img src="LOGO.png" alt="" className="img-logo"/>
        </div>
      </div>
    </>
  );
}

export default Header;
