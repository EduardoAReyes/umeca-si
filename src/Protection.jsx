import React from "react";
import { Outlet, Navigate } from "react-router-dom";

/**
 * 
 * Codigo para que nostros podamos proteger las rutas de nuestro sistema
 * (pendiente a implementar)
 */

function Protection({ hijo }) {
  console.log("En Protection" + hijo);
  let auth = localStorage.getItem("token");
  let auth2 = localStorage.getItem("token2");
  return auth || auth2 ? <Outlet /> : <Navigate to="/" />;
}

export default Protection;
