import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Login_Componentes/Login";
import Protection from "./Protection";
import Imputados from "./Imputados/Imputados";
import Empleados from "./Empleados/Empleados";
import Sobreseguimiento from "./Sobreseguimiento/Sobreseguimiento";
import Menu from "./Menu/Menu";
/**
 * Este es el componente principal del proyecto, donde se crean las rutas de todo el sistema,
 * para poder realizar nuestro loging funcional.
 */

function App() {
  const [logueado, setLogueado] = useState(false);

  const estado = (validado) => {
    setLogueado(validado);
  };
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login estado={estado} />} />
          <Route element={<Protection hijo={logueado} />}>
            <Route path="/Menu" element={<Menu />} />
            <Route path="/Imputados" element={<Imputados />} />
            <Route path="/Empleados" element={<Empleados />} />
            <Route path="/Sobreseguimiento" element={<Sobreseguimiento />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

// import React, { useState, useEffect } from "react";

// const App = () => {
//   const [count, setCount] = useState(0);

//   const handleClick = () => {
//     setCount(count + 1); // Actualización del estado (no instantánea)
//     console.log(count); // Esto no mostrará el nuevo valor inmediatamente
//   };

//   handleClick();
//   return (
//     <div>
//       <p>Count: {count}</p>
//       <button onClick={handleClick}>Increment</button>
//     </div>
//   );
// };

// export default App;
