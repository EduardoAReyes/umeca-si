import "../style/form.css";
import React from "react";
// import InputImputados from "./Inputs/InputImputados";
import InputImputados from "../Inputs/InputImputados";
import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";
import Axios from "axios";
import ModalT_Imputados from "./ModalT_Imputados";
import ModalCapturaImpu from "./ModalCapturaImpu";
import Modal from "react-bootstrap/Modal";
// import InputActualizarImpu from "./Inputs/InputActualizarImpu";
import InputActualizarImpu from "../Inputs/InputActualizarImpu";
// import ModalT_ActualizarImpu from "./ModalT_ActualizarImpu";

import InputActualizarEdad from "../Inputs/formulario_validaciones_update/InputActualizarEdad";
import InputActualizarTel from "../Inputs/formulario_validaciones_update/InputActualizarTel";
import InputActualizarEstado from "../Inputs/formulario_validaciones_update/InputActualizarEstado";
import InputActualizarCURP from "../Inputs/formulario_validaciones_update/InputActualizarCURP";
import InputActualizarGenero from "../Inputs/formulario_validaciones_update/InputActualizarGenero";
function ModalT_ActualizarImpu({ show, onHide, imputadosData }) {
  const [nombre, setNombre] = useState("");
  const [apellido_Paterno, setApellido_Paterno] = useState("");
  const [apellido_Materno, setApellido_Materno] = useState("");
  const [edad, setEdad] = useState("");
  const [genero, setGenero] = useState("");
  const [domicilio, setDomicilio] = useState("");
  const [telefono, setTelefono] = useState("");
  const [delito, setDelito] = useState("");
  const [estado, setEstado] = useState("");
  const [curp, setCurp] = useState("");

  const [modalT_ImputadosShow, setModalT_ImputadosShow] = useState(false);
  const [modalT_ActualizarImpuShow, setModalT_ActualizarImpuShow] =
    useState(false);

  const [valido, setValido] = useState(false);
  const [limpiar, setLimpiar] = useState(null);

  const actualizarImputado = () => {
    Axios.post("http://localhost:3000/actualizarImputado", {
      id_imputado: imputadosData.id_imputado,
      nombre,
      apellido_Paterno,
      apellido_Materno,
      edad,
      genero,
      domicilio,
      telefono,
      delito,
      estado,
      curp,
    })
      .then(() => {
        onHide();
      })
      .catch((err) => {
        console.log("Error en actualizar", err);
      });
  };

  return (
    <>
      <Modal
        show={show}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title>Actualizar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="col-md-9 cont2">
              <div className="formulario">
                {/* <h2>Ingregar Imputado</h2> */}
                <form onSubmit={actualizarImputado}>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarImpu
                          setCampo={setNombre}
                          place={imputadosData.nombre}
                          nombre="Nombre: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarImpu
                          setCampo={setApellido_Paterno}
                          place={imputadosData.apellido_Paterno}
                          nombre="Apellido Paterno: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarImpu
                          setCampo={setApellido_Materno}
                          place={imputadosData.apellido_Materno}
                          nombre="Apellido Materno: "
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <InputActualizarEdad
                        setCampo={setEdad}
                        place={imputadosData.edad}
                        nombre="Edad: "
                      />
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarGenero
                          setCampo={setGenero}
                          place={imputadosData.genero}
                          campo={genero}
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarImpu
                          setCampo={setDomicilio}
                          place={imputadosData.domicilio}
                          nombre="Domicilio: "
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarTel
                          setCampo={setTelefono}
                          place={imputadosData.telefono}
                          nombre="Telefono: "
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarImpu
                          setCampo={setDelito}
                          place={imputadosData.delito}
                          nombre="Delito: "
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarEstado
                          setCampo={setEstado}
                          place={imputadosData.estado}
                          campo={estado}
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <InputActualizarCURP
                          setCampo={setCurp}
                          place={imputadosData.curp}
                          nombre="CURP: "
                        />
                      </div>
                    </div>
                  </div>
                  <div className="botones">
                    <button
                      type="submit"
                      className="btn btn-secondary"
                      onClick={() => {
                        actualizarImputado();
                      }}
                    >
                      Actualizar
                    </button>
                    <button
                      type="submit"
                      className="btn btn-secondary"
                      onClick={(e) => {
                        e.preventDefault();
                        onHide();
                      }}
                    >
                      Cerrar
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalT_ActualizarImpu;
