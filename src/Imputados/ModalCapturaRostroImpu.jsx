import React from "react";
import Modal from "react-bootstrap/Modal";
import CapturaRostroImpu from "./CapturaRostroImpu";

function ModalCapturaRostroImpu({
  show,
  onHide,
  curp,
  ingresarImputado,
}) {
  return (
    <>
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title>Capturar Imputado</Modal.Title>
        </Modal.Header>
        <CapturaRostroImpu
          onHide={onHide}
          curp={curp}
          ingresarImputado={ingresarImputado}
        />
        {/* <Modal.Body></Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => {
              onHide();
            }}
          >
            Cerrar
          </button>
        </Modal.Footer> */}
      </Modal>
    </>
  );
}

export default ModalCapturaRostroImpu;
