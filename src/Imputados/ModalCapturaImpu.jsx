import React, { useRef } from "react";
import Webcam from "react-webcam";
import { useState } from "react";
import Axios from "axios";

const videoConstraints = {
  width: 1280,
  height: 720,
  facingMode: "user",
};

function ModalCapturaImpu() {
  const webcamRef = useRef();

  const [webUrl, setWebUrl] = useState(null);
  const [cargando, setCargando] = useState(false);

  let rostros = [];

  const capture = React.useCallback(() => {
    if (cargando) return;

    for (let i = 0; i < 3; i++) {
      const imageSrc = webcamRef.current.getScreenshot();
      rostros.push(imageSrc);
    }
    // setWebUrl(imageSrc);
    Axios.post("http://localhost:3000/guardarCaptura", {
      imagenes: rostros,
    })
      .then((res) => {
        console.log(res);
        rostros = [];
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setCargando(false);
      });
  }, [webcamRef]);

  return (
    <>
      <Webcam
        audio={false}
        height={720}
        ref={webcamRef}
        screenshotFormat="image/jpg"
        width={1280}
        videoConstraints={videoConstraints}
      />
      <button
        onClick={() => {
          setCargando(true);
          capture();
        }}
        disabled={cargando}
      >
        Capture photo
      </button>
      <div>{/* <img src={webUrl} alt="" name="image" /> */}</div>
    </>
  );
}

export default ModalCapturaImpu;
