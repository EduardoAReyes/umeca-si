import React from "react";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/table";
import { useEffect, useState } from "react";
import Axios from "axios";
import { Button } from "bootstrap";

function ModalT_Imputados({
  show,
  onHide,
  setModalActualizar,
  setImputadosData,
}) {
  const [ultimoCampo, setUltimoCampo] = useState([]);
  const [cambio, setCambio] = useState(false);

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarImputados")
      .then((res) => {
        setUltimoCampo(res.data);
      })
      .catch((err) => {
        console.log("Error en consulta de imputados: ", err);
      });
  }, []);

  useEffect(() => {
    Axios.get("http://localhost:3000/consultarImputados")
      .then((res) => {
        setUltimoCampo(res.data);
      })
      .catch((err) => {
        console.log("Error en consulta de imputados: ", err);
      })
      .finally(() => {
        setCambio(false);
      });
  }, [cambio]);

  const actualizarImputados = (
    id_imputado,
    nombre,
    apellido_Paterno,
    apellido_Materno,
    edad,
    genero,
    domicilio,
    telefono,
    delito,
    estado,
    curp
  ) => {
    const datos = {
      id_imputado,
      nombre,
      apellido_Paterno,
      apellido_Materno,
      edad,
      genero,
      domicilio,
      telefono,
      delito,
      estado,
      curp,
    };
    setImputadosData(datos);
  };

  const eliminarImputado = (id_imputado, curp) => {
    if (id_imputado !== "" && curp !== "") {
      Axios.post("http://localhost:3000/eliminarImputado", {
        id_imputado,
        curp,
      })
        .then(() => {
          setCambio(true);
        })
        .catch((err) => {
          console.log("Erro en consulta de imputados: ", err);
        });
    } else {
      return;
    }
  };

  return (
    <>
      <style>
        {`
      .tablaImp{
        max-height:300px;
        overflow-y:auto;
      }
      `}
      </style>
      <Modal
        show={show}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            IMPUTADOS
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="tablaImp">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>N.Caso</th>
                  <th>Nombre</th>
                  <th>Apellido paterno</th>
                  <th>Apellido materno</th>
                  <th>Edad</th>
                  <th>Genero</th>
                  <th>Domicilio</th>
                  <th>Telefóno</th>
                  <th>Delito</th>
                  <th>Estado</th>
                  <th>CURP</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {ultimoCampo.map((val) => (
                  <tr key={val.id_imputado}>
                    <td>{val.id_imputado}</td>
                    <td>{val.nombre}</td>
                    <td>{val.apellido_Paterno}</td>
                    <td>{val.apellido_Materno}</td>
                    <td>{val.edad}</td>
                    <td>{val.genero}</td>
                    <td>{val.domicilio}</td>
                    <td>{val.telefono}</td>
                    <td>{val.delito}</td>
                    <td>{val.estado}</td>
                    <td>{val.curp}</td>
                    <td>
                      <button
                        type="button"
                        onClick={() => {
                          actualizarImputados(
                            val.id_imputado,
                            val.nombre,
                            val.apellido_Paterno,
                            val.apellido_Materno,
                            val.edad,
                            val.genero,
                            val.domicilio,
                            val.telefono,
                            val.delito,
                            val.estado,
                            val.curp
                          );
                          onHide();
                          setModalActualizar(true);
                        }}
                      >
                        Actualizar
                      </button>
                    </td>
                    <td>
                      <button
                        type="button"
                        onClick={(e) => {
                          e.preventDefault();
                          eliminarImputado(val.id_imputado, val.curp);
                        }}
                      >
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => {
              onHide();
            }}
          >
            Cerrar
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalT_Imputados;
