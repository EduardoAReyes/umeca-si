import "../style/form.css";
import React from "react";
import InputImputados from "../Inputs/InputImputados";
import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";
import Axios from "axios";
import ModalT_Imputados from "./ModalT_Imputados";
import ModalCapturaImpu from "./ModalCapturaImpu";
import ModalT_ActualizarImpu from "./ModalT_ActualizarImpu";
import ModalCapturaRostroImpu from "./ModalCapturaRostroImpu";

import InputCargoImp from "../Inputs/formulario_validaciones/InputCargoImp";
import InputEdad from "../Inputs/formulario_validaciones/InputEdad";
import InputTelefono from "../Inputs/formulario_validaciones/InputTelefono";
import InputCURP from "../Inputs/formulario_validaciones/InputCURP";
import InputGenero from "../Inputs/formulario_validaciones/InputGenero";
function FormularioImputados() {
  const navegar = useNavigate();

  const [imputadosData, setImputadosData] = useState("");

  const [nombre, setNombre] = useState("");
  const [apellido_Paterno, setApellido_Paterno] = useState("");
  const [apellido_Materno, setApellido_Materno] = useState("");
  const [edad, setEdad] = useState("");
  const [genero, setGenero] = useState("");
  const [domicilio, setDomicilio] = useState("");
  const [telefono, setTelefono] = useState("");
  const [delito, setDelito] = useState("");
  const [estado, setEstado] = useState("");
  const [curp, setCurp] = useState("");

  const [modalCapturaRostroImpuShow, setModalCapturaRostroImpuShow] =
    useState(false);

  const [modalT_ImputadosShow, setModalT_ImputadosShow] = useState(false);
  const [modalT_ActualizarImpuShow, setModalT_ActualizarImpuShow] =
    useState(false);

  const [valido, setValido] = useState(false);
  const [limpiar, setLimpiar] = useState(null);

  const ingresarImputado = (e) => {
    Axios.post("http://localhost:3000/ingresarImputado", {
      nombre,
      apellido_Paterno,
      apellido_Materno,
      edad,
      genero,
      domicilio,
      telefono,
      delito,
      estado,
      curp,
    })
      .then((res) => {
        setModalCapturaRostroImpuShow(false);
        window.location.reload();
      })
      .catch((err) => {
        console.log("Hubo un error", err);
      });
  };

  const prueba = () => {
    console.log("Hola mundo desde captura de rostro");
  };

  const capturaRostro = () => {
    if (
      nombre !== "" ||
      apellido_Paterno !== "" ||
      apellido_Materno !== "" ||
      edad !== "" ||
      genero !== "" ||
      domicilio !== "" ||
      telefono !== "" ||
      delito !== "" ||
      estado !== "" ||
      curp !== ""
    ) {
      setModalCapturaRostroImpuShow(true);
    } else {
      return;
    }
  };

  return (
    <>
      <style>{`
      .container{
        display:flex;
        justify-content:center;
        align-items:center;
      }
      .formulario {
        background-color: #fff;
        border-radius: 10px;
        padding: 20px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .form-group {
        margin-bottom: 20px;
    }

    label {
        font-weight: bold;
    }

    .flecha-a-menu{
      position:fixed;
      width:50px;
    }

    .botones{
      display: flex;
      justify-content: space-around;
      align-items: center;
  }

      `}</style>
      <Link to="/Menu">
        <img src="arrow.svg" className="flecha-a-menu" />
      </Link>
      <ModalT_Imputados
        show={modalT_ImputadosShow}
        onHide={() => setModalT_ImputadosShow(false)}
        setImputadosData={setImputadosData}
        setModalActualizar={setModalT_ActualizarImpuShow}
      />

      <ModalT_ActualizarImpu
        show={modalT_ActualizarImpuShow}
        onHide={() => setModalT_ActualizarImpuShow(false)}
        imputadosData={imputadosData}
        setImputadosData={setImputadosData}
      />

      <ModalCapturaRostroImpu
        show={modalCapturaRostroImpuShow}
        onHide={() => setModalCapturaRostroImpuShow(false)}
        curp={curp}
        ingresarImputado={() => {
          ingresarImputado();
        }}
      />

      <div className="container">
        <div className="col-md-9 cont2">
          <div className="formulario">
            <h2>Registrar Imputado</h2>
            <form>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <InputImputados place="Nombre" setCampo={setNombre} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputImputados
                      place="Apellido Paterno"
                      setCampo={setApellido_Paterno}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputImputados
                      place="Apellido Materno"
                      setCampo={setApellido_Materno}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputEdad place="Edad" setCampo={setEdad} />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputGenero place="Genero" setCampo={setGenero} />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputImputados place="Domicilio" setCampo={setDomicilio} />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputTelefono place="Telefóno" setCampo={setTelefono} />
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <InputImputados place="Delito" setCampo={setDelito} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputCargoImp place="Estado" setCampo={setEstado} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <InputCURP place="CURP" setCampo={setCurp} />
                  </div>
                </div>
              </div>
              <div className="botones">
                <button
                  type="submit"
                  className="btn btn-secondary"
                  onClick={(e) => {
                    e.preventDefault();
                    capturaRostro();
                  }}
                >
                  Registrar
                </button>
                <button
                  type="submit"
                  className="btn btn-secondary"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalT_ImputadosShow(true);
                  }}
                >
                  Tabla
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default FormularioImputados;
