import React, { useRef, useState } from "react";
import Webcam from "react-webcam";

import Modal from "react-bootstrap/Modal";
import Axios from "axios";
const videoConstraints = {
  width: 620,
  height: 460,
  facingMode: "user",
};

function CapturaRostroImpu({
  onHide,
  curp,
  ingresarImputado,
}) {
  const webcamRef = useRef();

  const [webUrl, setWebUrl] = useState(null);
  const [cargando, setCargando] = useState(false);

  let rostros = [];

  const capture = React.useCallback(() => {
    if (cargando) return;

    for (let i = 0; i < 3; i++) {
      const imageSrc = webcamRef.current.getScreenshot();
      rostros.push(imageSrc);
    }
    // setWebUrl(imageSrc);
    Axios.post("http://localhost:3000/capturarRostroImpu", {
      imagenes: rostros,
      curp
    })
      .then((res) => {
        rostros = [];
        ingresarImputado();
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setCargando(false);
      });
  }, [webcamRef]);

  return (
    <>
      <style>{`
    .webcam-impu{
      margin: 0;
      padding: 0;
      display: flex;
      justify-content: center;
      align-items: center;
  }
    `}</style>
      <Modal.Body>
        <div className="webcam-impu">
          <Webcam
            audio={false}
            height={460}
            ref={webcamRef}
            screenshotFormat="image/jpg"
            width={620}
            videoConstraints={videoConstraints}
          />
        </div>
        <div>{/* <img src={webUrl} alt="" name="image" /> */}</div>
      </Modal.Body>

      <Modal.Footer>
        <button
          onClick={() => {
            setCargando(true);
            capture();
          }}
          disabled={cargando}
        >
          Capturar rostros
        </button>
        <button
          onClick={() => {
            onHide();
          }}
        >
          Cerrar
        </button>
      </Modal.Footer>
    </>
  );
}

export default CapturaRostroImpu;
