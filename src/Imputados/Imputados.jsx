import React from "react";
import Header from "../Header";
import FormularioImputados from "./FormularioImputados";
function Imputados() {


  return (
    <>
      <Header encabezado="IMPUTADOS" />
      <FormularioImputados />
    </>
  );
}

export default Imputados;
