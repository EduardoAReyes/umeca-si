import React from "react";
import { useEffect } from "react";
function InputActualizarGenero({ setCampo, place, campo }) {
  useEffect(() => {
    setCampo(place);
  }, []);

  const handleChange = (e) => {
    let dato = e.target.value;
    setCampo(dato);
  };

  return (
    <select name="Genero" className="form-control" onChange={handleChange}>
      <option value="" selected hidden>
        {campo}
      </option>
      <option value="Masculino">Masculino</option>
      <option value="Femenino">Femenino</option>
    </select>
  );
}

export default InputActualizarGenero;
