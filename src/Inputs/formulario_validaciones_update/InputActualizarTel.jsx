import React from "react";
import { useEffect, useState } from "react";
function InputActualizarTel({ setCampo, place, nombre }) {
  const [valorI, setValorI] = useState(place);
  const [value, setValue] = useState("");

  useEffect(() => {
    value ? setCampo(value) : setCampo(valorI);
  }, [value, setCampo]);

  const handleChange = (e) => {
    let dato = e.target.value;
    if (/^\d*$/.test(dato) && dato.length <= 10) {
      setValue(dato);
    } else {
      setValue("");
    }
  };
  return (
    <>
      <input
        type="text"
        className="form-control"
        value={value}
        placeholder={nombre + place}
        onChange={handleChange}
      />
    </>
  );
}

export default InputActualizarTel;
