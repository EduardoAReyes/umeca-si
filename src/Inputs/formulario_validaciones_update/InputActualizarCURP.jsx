import React from "react";
import { useEffect, useState } from "react";
function InputActualizarCURP({ setCampo, place, nombre }) {
  const [valorI, setValorI] = useState(place);
  const [value, setValue] = useState("");

  useEffect(() => {
    value ? setCampo(value) : setCampo(valorI);
  }, [value, setCampo]);

  const handleChange = (e) => {
    let dato = e.target.value;
    if (dato.length <= 18) {
      setValue(dato);
    } else {
      setValue("");
    }
  };
  return (
    <>
      <input
        type="text"
        className="form-control"
        value={value}
        placeholder={nombre + place}
        onChange={handleChange}
      />
    </>
  );
}

export default InputActualizarCURP;
