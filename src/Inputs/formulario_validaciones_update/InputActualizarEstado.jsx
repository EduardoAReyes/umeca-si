import React from "react";
import { useEffect, useState } from "react";
function InputActualizarEstado({ setCampo, place, campo }) {
  useEffect(() => {
    setCampo(place);
  }, []);

  const handleChange = (e) => {
    let dato = e.target.value;
    setCampo(dato);
  };
  return (
    <>
      <select name="Estados" className="form-control" onChange={handleChange}>
        <option value="" hidden selected>
          {campo}
        </option>
        <option value="Incumpliendo">Incumpliendo</option>
        <option value="Cumpliendo">Cumpliendo</option>
      </select>
    </>
  );
}

export default InputActualizarEstado;
