import React from "react";
import { useEffect, useState } from "react";
function InputActualizarCargo({ setCampo, place, campo }) {
  useEffect(() => {
    setCampo(place);
  }, []);

  const handleChange = (e) => {
    let dato = e.target.value;
    setCampo(dato);
  };
  return (
    <>
      <select name="Cargos" className="form-control" onChange={handleChange}>
        <option value="" hidden selected>
          {campo}
        </option>
        <option value="Supervisor">Supervisor</option>
        <option value="Empleado">Empleado</option>
        <option value="Externo">Externo</option>{" "}
      </select>
    </>
  );
}

export default InputActualizarCargo;
