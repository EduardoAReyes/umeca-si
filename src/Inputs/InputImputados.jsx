import React from "react";
function InputImputados({ place, setCampo }) {
  return (
    <>
      <input
        type="text"
        required
        autoComplete="off"
        placeholder={place}
        className="form-control"
        onChange={(e) => {
          setCampo(e.target.value);
        }}
      />
    </>
  );
}

export default InputImputados;
