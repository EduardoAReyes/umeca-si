import React from "react";
import { useEffect, useState } from "react";
function InputActualizarImpu({ setCampo, place, nombre }) {
  const [valorI, setValorI] = useState(place);
  useEffect(() => {
    setCampo(valorI);
  }, []);

  return (
    <>
      <input
        type="text"
        className="form-control"
        placeholder={nombre + place}
        onChange={(e) => {
          let dato = e.target.value;
          if (dato !== "") {
            setCampo(dato);
          } else {
            setCampo(valorI);
          }
        }}
      />
    </>
  );
}

export default InputActualizarImpu;
