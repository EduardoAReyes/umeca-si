import React from "react";

function InputGenero({ setCampo }) {
  return (
    <select
      name="Género"
      className="form-control"
      onChange={(e) => {
        const genero = e.target.value;
        setCampo(genero);
      }}
    >
      <option value="" selected hidden>
        Genero
      </option>
      <option value="Masculino">Masculino</option>
      <option value="Femenino">Femenino</option>
    </select>
  );
}

export default InputGenero;
