import React from "react";
function InputCargo({ setCampo }) {
  return (
    <>
      <select
        name="Cargos"
        className="form-control"
        onChange={(e) => {
          const cargo = e.target.value;
          setCampo(cargo);
          console.log(cargo);
        }}
      >
        <option selected hidden value="">
          Cargo
        </option>
        <option value="Supervisor">Supervisor</option>
        <option value="Empleado">Empleado</option>
        <option value="Externo">Externo</option>
      </select>
    </>
  );
}

export default InputCargo;
