import React, { useState, useEffect } from "react";

function InputCURP({ place, setCampo }) {
  const [value, setValue] = useState('');

  useEffect(() => {
    setCampo(value);
  }, [value, setCampo]);

  const handleChange = (e) => {
    const newValue = e.target.value;
    // Validar que el nuevo valor solo contenga números y tenga un máximo de 10 caracteres
    if (newValue.length <= 18) {
      setValue(newValue);
    }
  };

  return (
    <input
      type="text"
      required
      autoComplete="off"
      placeholder={place}
      className="form-control"
      value={value}
      onChange={handleChange}
    />
  );
}

export default InputCURP;
