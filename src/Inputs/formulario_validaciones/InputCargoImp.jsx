import React from "react";
import { useState } from "react";
function InputCargoImp({ setCampo }) {
  return (
    <>
      <select
        name="Estados"
        className="form-control"
        onChange={(e) => {
          const estado = e.target.value;
          setCampo(estado);
        }}
      >
        <option selected hidden value="">
          Cargo
        </option>
        <option value="Cumpliendo">Cumpliendo</option>
        <option value="Incumpliendo">Incumpliendo</option>
      </select>
    </>
  );
}

export default InputCargoImp;
