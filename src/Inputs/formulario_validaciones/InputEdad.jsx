import React, { useState, useEffect } from "react";

function InputEdad({ place, setCampo }) {
  const [value, setValue] = useState('');

  useEffect(() => {
    setCampo(value);
  }, [value, setCampo]);

  const handleChange = (e) => {
    const newValue = e.target.value;
    // Validar que el nuevo valor solo contenga números y tenga un máximo de 10 caracteres
    if (/^\d*$/.test(newValue) && newValue.length <= 2) {
      setValue(newValue);
      console.log(newValue); // Mostrar el valor en la consola
    }
  };

  return (
    <input
      type="text"
      required
      autoComplete="off"
      placeholder={place}
      className="form-control"
      value={value}
      onChange={handleChange}
    />
  );
}

export default InputEdad;
